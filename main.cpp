#include <iostream>
#include <fstream>
#include <stdexcept>
#include <cstring>
#include <map>
#include <utility>
#include <vector>
#include <sstream>
#include <iterator>

using namespace std;

const int maxASCII = 128;
class Automat
{
private:
    char m_stareInitiala{};
    bool m_esteStareFinala[maxASCII] {};
    map< pair<char, string>, char > m_functieTranzitie;

public:
    Automat() {}
    ~Automat() {}
    bool include(const vector<string>& cuvant);

    friend istream& operator>>(istream& in, Automat &automat);
};

bool Automat::include(const vector<string>& cuvant)
{
    unsigned char stareCurenta = m_stareInitiala;
    for(unsigned int i=0; i<cuvant.size(); i++)
    {
        string litera = cuvant[i];
        pair<char, string> stareUrmatoare = make_pair(stareCurenta, litera);
        try
        {
            m_functieTranzitie.at(stareUrmatoare);
        }
        catch(out_of_range)
        {
            cout << "Tranzitia (" << stareCurenta << ", " << litera << ") nu este definita in automat." << '\n';
            return false;
        }
        stareCurenta = m_functieTranzitie[stareUrmatoare];
    }
    return m_esteStareFinala[stareCurenta];
}

istream& operator>>(istream& in, Automat &automat)
{
    in >> automat.m_stareInitiala;
    int numarStariFinale;
    in >> numarStariFinale;
    for(int i=0; i<numarStariFinale; i++)
    {
        unsigned char stareFinala;
        in >> stareFinala;
        automat.m_esteStareFinala[stareFinala] = true;
    }
    int numarTranzitii;
    in >> numarTranzitii;
    for(int i=0; i<numarTranzitii; i++)
    {
        char stareCurenta, stareUrmatoare;
        string litera;
        in >> stareCurenta >> litera >> stareUrmatoare;
        automat.m_functieTranzitie[{stareCurenta, litera}] = stareUrmatoare;
    }
    return in;
}

int main()
{
    Automat automat;
    ifstream fin("automat.in");
    fin >> automat;
    string buffer;
    getline(cin, buffer);
    istringstream iss(buffer);
    const vector<string> cuvant{istream_iterator<string>{iss},
                                istream_iterator<string>{}};
    if(automat.include(cuvant))
        cout<<"Cuvantul apartine limbajului recunoscut de automat.";
    else
        cout<<"Cuvantul nu apartine limbajului recunoscut de automat.";
    return 0;
}
